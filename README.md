### Docus

My various documents. 
This repo exist for the purpose of maintaining it's embedded wiki.
On the left menu `Plan` -> `Wiki`.
The advantage is that you can edit all documents directly on gitlab website.
Now you can do it also for normal repo files as well, but you can only edit and see preview of a single file.

>Important difference is that related links to the documents stored on the special `wiki` repo work as if you were browsing properly hosted html (or html dynamically generated 
from markdown `*.md` files).

