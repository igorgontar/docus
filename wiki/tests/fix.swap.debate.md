### Swap modelling with FIX v4.3 protocol 
#### NewOrderSingle

```mermaid
graph LR;
  EURCHF --- Side=B --- OrderQty=1M --- SettDate --- d2(SettDate2) --- q2(OrdQty2);
  classDef far fill:#bbf,stroke:#f66,stroke-width:2px,color:#fff,stroke-dasharray: 5, 5;
  class d2,q2 far;
```    
#### NewOrderMultileg
```mermaid
graph LR;
  EURCHF --- Side=B --- mq1[OrderQty=1M] --- md1[SettDate] --- legs((legs));
  legs --> d1(SettDate) --- q1(OrdQty);
  legs --> d2(SettDate) --- q2(OrdQty);

  classDef near fill:#f96,color:#fff,stroke-width:2px;
  class mq1,md1,d1,q1 near;

  classDef far fill:#bbf,stroke:#f66,stroke-width:2px,color:#fff,stroke-dasharray: 5, 5;
  class d2,q2 far;
```    
