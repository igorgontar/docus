### Flow diagram

```mermaid
graph TD;
  NEW-->SENT;
  SENT--Notify-->REJ;
  SENT--Notify-->ACC;
  ACC-->PND;
  PND-->COM;
COM-->DONE;
REJ-->DONE;
DONE-->NEW;
```    
