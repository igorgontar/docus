### Gannt diagram for Sprint planning

```mermaid
gantt
dateFormat  YYYY-MM-DD
title Sprint 3 (21.04 - 05.05.2020)
excludes weekdays 2014-01-10

section A section
Migrate to new API           :done,    id0, 2020-04-21,2020-05-01
Fix logging issue            :active,  id1, 2020-04-21, 5d
Fin. Field definition        :active,  id2, 2020-04-21, 7d
Create Service Skeleton      :         id3, after id2, 3d
Configure AppPool            :         id4, after id3, 5d

section B section
Migrate to new API           :done,    id0, 2020-04-21,2020-05-01
Fix logging issue            :active,  id1, 2020-04-21, 5d
Fin. Field definition        :active,  id2, 2020-04-21, 7d
Create Service Skeleton      :         id3, after id2, 3d
Configure AppPool            :         id4, after id3, 4d
```
