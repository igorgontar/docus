### State diagram

```mermaid
stateDiagram
    [*] --> Received
    
    Received --> Sent
    Sent --> Accepted
    Sent --> Rejected
    Rejected --> Sent
    Accepted --> Pending
    Accepted --> Executed
    Accepted --> CancellRequested
    CancellRequested --> Cancelled
    Pending --> Executed
    Executed --> [*]
    ExecFailed --> [*]    
    Cancelled --> [*]    
    Rejected --> [*] 
```
